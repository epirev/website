from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

from projects.models import Project
from accounts.models import User

class CreateProjectForm(forms.ModelForm):
    widgets = { 'opensource': forms.RadioSelect, }
    class Meta:
        model = Project
        fields = [
                'name',
                'image',
                'short_description',
                'members',
                'opensource',
                'tech',
                ]

    class Media:
         css = {'all': ('/static/css/multiple-select.css',),}
         js = ('/static/js/jquery.multiple.select.js',)

class UpdateProjectForm(forms.ModelForm):
    widgets = { 'opensource': forms.RadioSelect, }
    class Meta:
        model = Project
        fields = [
                'description_markdown',
                'members',
                'image',
                'name',
                'short_description',
                'opensource',
                'end',
                ]

    class Media:
         css = {'all': ('/static/css/multiple-select.css',),}
         js = ('/static/js/jquery.multiple.select.js',)

