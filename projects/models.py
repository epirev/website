from django.utils.text import slugify
from django.db import models

from easy_thumbnails.fields import ThumbnailerImageField as ImageField

from epirev.utils import mdrenderer
from chinwag.models import Forum
from epirev.models import View

class Tech(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

class Project(models.Model):
    BOOL_CHOICES = ((True, 'Yes'), (False, 'No'))

    PROJECT_END_CHOICES = (
        ('F', 'Finished'),
        ('D', 'Dropped'),
    )

    name = models.CharField(max_length=50)
    creator = models.ForeignKey('accounts.User')
    slug_name = models.SlugField(unique=True, blank=True, editable=False)
    image = ImageField(upload_to="projects/project/images/",
            default="projects/project/images/default.png")
    short_description = models.TextField()

    description = models.TextField(blank=True, editable=False)
    description_markdown = models.TextField(blank=True)

    members = models.ManyToManyField('accounts.User',
            related_name='project_members')
    tech = models.ForeignKey(Tech, related_name='project_tech')
    opensource = models.BooleanField(default=True, choices=BOOL_CHOICES)
    pub_date = models.DateField('Date created', auto_now_add=True)
    last_update = models.DateTimeField('Last update', auto_now_add=True)

    forum = models.ForeignKey(Forum, related_name='project_forum')

    end = models.CharField(max_length=20, choices=PROJECT_END_CHOICES,
        blank=True, null=True, default=None)
    end_date = models.DateField(blank=True, null=True)
    visible = models.BooleanField(default=False, choices=BOOL_CHOICES)

    views = models.ManyToManyField(View, related_name='project_views')

    __was_end = None
    __was_visible = None

    def __init__(self, *args, **kwargs):
        super(Project, self).__init__(*args, **kwargs)
        self.__was_end = self.end
        self.__was_visible = self.visible

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug_name = slugify(self.name)
            forum = Forum(name=self.name)
            forum.save()
            self.forum = forum

        if (not self.pk or not self.__was_end) and self.end:
            self.end_date = timezone.now()

        if (not self.pk or self.__was_visible) and not self.visible:
            self.pub_date = timezone.now()

        self.last_update = timezone.now()
        self.description = mdrenderer.render(self.description_markdown)
        super(Project, self).save(*args, **kwargs)
        self.forum.project = self
        self.forum.save()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name', 'pub_date']
