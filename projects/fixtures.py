import random
from datetime import date, datetime

from django.utils.timezone import utc

from faker import Faker

from projects.models import Project, Tech
from accounts.models import User
from chinwag.models import Forum, Thread

TECHS = [
    'Unity3D',
    'Unity4',
    'Oculus SDK',
]

PROJECTS_TO_LOAD = 6
PROJECT_THREADS_TO_LOAD = 30
PROJECT_THREAD_VIEWS_TO_LOAD = 3000
PROJECT_THREAD_POSTS_TO_LOAD = 1000

BEGINNING = date.today().replace(day=1, month=1)
TODAY = date.today()

def get_random_date(start_date=BEGINNING, end_date=TODAY):
    return date.fromordinal(random.randint(start_date.toordinal(),
        end_date.toordinal()))

def load_techs():
    Tech.objects.all().delete()
    for tech in TECHS:
        t = Tech(name=tech)
        t.save()
    return (str(len(TECHS)) + ' techs loaded')

def load_projects(fake):
    Project.objects.all().delete()
    techs = list(Tech.objects.all())
    members = list(User.objects.filter(is_active=True, level__gte=3))
    for i in range(PROJECTS_TO_LOAD):
        project = Project(
                name = fake.word().capitalize(),
                short_description = fake.paragraph(),
                description_markdown = ' '.join(fake.paragraphs()),
                tech = random.choice(techs),
                opensource = bool(random.getrandbits(1)),
                visible = True,
                end = (random.choice(Project.PROJECT_END_CHOICES)[0] if
                        bool(random.getrandbits(1)) else None),
                creator = User.objects.get(username='toogy'),
                )
        project.save()
        project.members = random.sample(set(members), random.randint(2, 6))
        project.save()

    return (str(PROJECTS_TO_LOAD) + ' projects loaded')

def load_project_threads(fake):
    forums = list(Forum.objects.exclude(project__isnull=True))
    users = list(User.objects.all())
    for i in range(PROJECT_THREADS_TO_LOAD):
        forum = random.choice(forums)
        forum.threads.create(
            subject = fake.sentence(),
            forum = forum,
            author = random.choice(users),
        )
    return (str(PROJECT_THREADS_TO_LOAD) + ' project threads loaded')

def load_project_thread_views(fake):
    threads = list(Thread.objects.exclude(forum__project__isnull=True))
    users = list(User.objects.all())
    for i in range(PROJECT_THREAD_VIEWS_TO_LOAD):
        random.choice(threads).views.create(
            user = random.choice(users),
            ip = fake.ipv4(),
        )
    return (str(PROJECT_THREAD_VIEWS_TO_LOAD) + ' project thread views loaded')

def load_project_thread_posts(fake):
    threads = list(Thread.objects.exclude(forum__project__isnull=True))
    users = list(User.objects.all())
    for i in range(PROJECT_THREAD_POSTS_TO_LOAD):
        thread = random.choice(threads)
        thread.posts.create(
            markdown = ' '.join(fake.paragraphs()),
            author = random.choice(users),
            thread = thread,
        )
    return (str(PROJECT_THREAD_POSTS_TO_LOAD) + ' project thread posts loaded')

def load():
    fake = Faker()
    returns = []
    returns.append(load_techs())
    returns.append(load_projects(fake))
    returns.append(load_project_threads(fake))
    returns.append(load_project_thread_views(fake))
    returns.append(load_project_thread_posts(fake))
    return returns

