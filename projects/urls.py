from django.conf.urls import patterns, include, url
from django.contrib import admin

from projects.views import ProjectCreate, ProjectUpdate

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^all$', 'projects.views.projects', name='all'),
    url(r'^create$', ProjectCreate.as_view(), name='create'),
    url(r'^update/(?P<slug_name>[\w-]+)', ProjectUpdate.as_view(),
        name='update'),
    url(r'^(?P<slug_name>[\w-]+)$', 'projects.views.project', name='project'),
)
