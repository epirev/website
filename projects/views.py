from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView
from django import forms
from django.forms.models import modelform_factory
from django.utils import timezone
from django.conf import settings
from django.db.models import Count

from ipware.ip import get_ip

from projects.models import Project
from projects.forms import CreateProjectForm, UpdateProjectForm

def projects(request):
    if request.user.is_authenticated():
        all_projects = Project.objects \
            .annotate(num_views = Count('views')) \
            .select_related('tech') \
            .prefetch_related('members')
    else:
        all_projects = Project.objects \
            .filter(visible=True) \
            .annotate(num_views = Count('views')) \
            .select_related('tech') \
            .prefetch_related('members')

    old_projects = [p for p in all_projects if p.end]

    user_projects = []
    if request.user.is_authenticated():
        user_projects = [p for p in all_projects if not p in old_projects and
                request.user in p.members.all()]

    projects = [p for p in all_projects if not p.end and not p in
            user_projects]

    return render(request, 'projects/projects.html', {
        'projects': projects,
        'old_projects': old_projects,
        'user_projects': user_projects,
    })

def project(request, slug_name):
    try:
        project = Project.objects.prefetch_related('views').get(
                slug_name=slug_name)
    except Project.DoesNotExist:
        raise Http404()

    if not project.visible and not request.user.is_authenticated():
        return redirect('/login/?next=%s' % request.path)

    user = request.user if request.user.is_authenticated() else None
    ip = get_ip(request)
    if ip is not None:
        matched_views = [view for view in project.views.all()
                if view.ip == ip][:1]
        if matched_views:
            elapsed = (timezone.now() - matched_views[0].date).seconds
            if elapsed > settings.PROJECTS_TIME_BETWEEN_VIEWS:
                project.views.create(ip=ip, user=user)
        else:
            project.views.create(ip=ip, user=user)

    return render(request, 'projects/project.html', { 'project': project, })

class ProjectCreate(CreateView):
    model = Project
    template_name = 'projects/create.html'
    success_url = reverse_lazy('projects')
    form_class = CreateProjectForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProjectCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.creator = self.request.user
        obj.save()
        return HttpResponseRedirect(self.success_url)

class ProjectUpdate(UpdateView):
    model = Project
    template_name = 'projects/update.html'
    form_class = UpdateProjectForm

    def get_object(self, queryset=None):
        obj = Project.objects.get(slug_name=self.kwargs['slug_name'])
        return obj

    def get_success_url(self):
        return reverse_lazy('projects:project',
                kwargs={'slug_name': self.object.slug_name})
