from django.contrib import admin
from projects import models

class ProjectAdmin(admin.ModelAdmin):
    exclude = ('slug_name', 'description', )

admin.site.register(models.Tech)
admin.site.register(models.Project, ProjectAdmin)

