from datetime import datetime

from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db.models import Max, Sum
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from chinwag.models import Forum, Thread, Post, LastSeen

class ForumsAwareMixin:
    def get_forums(self):
        forums = Forum.objects.all().prefetch_related('project__members',
                'threads', 'threads__last_post', 'threads__author')
        projects_forums = []
        user_projects_forums = []
        other_forums = []
        last_seens = LastSeen.objects.filter(user=self.request.user).values(
                'forum', 'date', 'thread__pk')
        for forum in forums:
            forum.pins = 0
            if forum.threads.count():
                for thread in forum.threads.all():
                    last_seen = next((x for x in last_seens \
                            if x['thread__pk'] == thread.pk), None)
                    if (not last_seen and \
                            thread.last_post.date > self.request.user.joined) \
                            or thread.last_post.date > last_seen['date']:
                        forum.pins += 1
            if forum.project:
                if self.request.user in forum.project.members.all() and not \
                        forum.project.end:
                    user_projects_forums.append(forum)
                else:
                    projects_forums.append(forum)
            else:
                other_forums.append(forum)
        projects_forums = user_projects_forums + projects_forums
        return (projects_forums, other_forums)

    def get_context_data(self, **kwargs):
        ctx = super(ForumsAwareMixin, self).get_context_data(**kwargs)
        projects_forums, other_forums = self.get_forums()
        ctx['projects_forums'] = projects_forums
        ctx['other_forums'] = other_forums
        return ctx

class HotViewMixin(TemplateView):
    model = Thread

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HotViewMixin, self).dispatch(*args, **kwargs)

    def get_hot_threads(self):
        threads = []
        last_seens = LastSeen.objects.filter(user=self.request.user).values(
                'forum', 'date', 'thread')
        for follow in self.request.user.follows \
                .filter(canceled=False) \
                .select_related('thread', 'thread__last_post',
                                'thread__author', 'thread__forum',
                                'thread__last_post__author')[:10]:
            thread = follow.thread
            candidats = [x for x in last_seens if x['thread'] == thread.pk]
            if not candidats or thread.last_post.date > candidats[0]['date']:
                thread.new = True
            threads.append(thread)
        return threads

    def get_context_data(self, **kwargs):
        ctx = super(HotViewMixin, self).get_context_data(**kwargs)
        threads = self.get_hot_threads()
        ctx['threads'] = threads
        return ctx

class HotView(ForumsAwareMixin, HotViewMixin):
    template_name = 'chinwag/forum.html'

    def get_context_data(self, **kwargs):
        ctx = super(HotView, self).get_context_data(**kwargs)
        return ctx

class HotAjaxView(HotViewMixin):
    template_name = 'chinwag/forum.render.html'

    def get_context_data(self, **kwargs):
        ctx = super(HotAjaxView, self).get_context_data(**kwargs)
        return ctx

class ForumViewMixin(TemplateView):
    model = Thread

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ForumViewMixin, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(ForumViewMixin, self).get_context_data(**kwargs)
        forum = Forum.objects.get(slug_name=kwargs['slug_name'])
        thread_list = Thread.objects \
            .filter(forum=forum) \
            .select_related('last_post', 'last_post__author') \
            .prefetch_related('views')
        paginator = Paginator(thread_list, 10)
        page = self.request.GET.get('page')
        try:
            threads = paginator.page(page)
        except PageNotAnInteger:
            threads = paginator.page(1)
        except EmptyPage:
            threads = paginator.page(paginator.num_pages)
        last_seens = LastSeen \
            .objects.filter(user = self.request.user, forum = forum) \
            .select_related('thread') \
            .values('thread', 'date')
        for thread in threads:
            last_seen = next((x for x in last_seens if \
                    x['thread'] == thread.pk), None)
            if not last_seen or thread.last_post.date > last_seen['date']:
                thread.new = True
        ctx['threads'] = threads
        ctx['current_forum'] = forum
        return ctx

class ForumView(ForumsAwareMixin, ForumViewMixin):
    template_name = 'chinwag/forum.html'

    def get_context_data(self, **kwargs):
        ctx = super(ForumView, self).get_context_data(**kwargs)
        return ctx

class ForumAjaxView(ForumViewMixin):
    template_name = 'chinwag/forum.render.html'

    def get_context_data(self, **kwargs):
        ctx = super(ForumAjaxView, self).get_context_data(**kwargs)
        return ctx

class ThreadView(ForumsAwareMixin, TemplateView):
    model = Thread
    template_name = 'chinwag/thread.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ThreadView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):

        # Getting the thread and its related data
        # comprising the thread posts (what's of interest here)
        thread = Thread.objects \
            .select_related('forum') \
            .prefetch_related('posts', 'posts__author') \
            .get(slug_subject=kwargs['slug_subject'], \
                 forum__slug_name=kwargs['slug_name'])

        now = timezone.now()

        # Getting the date the user was seen watching this thread for the last
        # time. If she never watched it before, we save a new LastSeen object
        try:
            last_seen = LastSeen.objects.get(user=self.request.user,
                    thread=thread)
        except LastSeen.DoesNotExist:
            last_seen = LastSeen(
                user = self.request.user,
                forum = thread.forum,
                thread = thread,
                date = now,
            )

        # Paginating the thread's posts
        posts_list = thread.posts.all()
        paginator = Paginator(posts_list, self.request.user.posts_per_page)
        page = self.request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)

        # Marking the posts as new if the user never saw them
        for post in posts:
            if last_seen.date == now or post.date > last_seen.date:
                post.new = True

        # Updating the last seen date no matter what
        last_seen.date = now
        last_seen.save()

        # This will get the
        ctx = super(ThreadView, self).get_context_data(**kwargs)
        ctx['thread'] = thread
        ctx['current_forum'] = thread.forum
        return ctx
