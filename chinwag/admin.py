from django.contrib import admin
from chinwag import models

admin.site.register(models.Forum)
admin.site.register(models.Thread)
admin.site.register(models.Post)

# Register your models here.
