import random
import pytz
from datetime import date, datetime
from django.utils import timezone

from faker import Faker

from accounts.models import User
from epirev.models import View
from chinwag.models import Forum, Thread, Post

FORUMS = [
    ['general', 'News'],
    ['general', 'Ideas'],
    ['general', 'Stuff'],
    ['office', 'Office'],
]

THREADS_TO_LOAD = 60
THREAD_VIEWS_TO_LOAD = 2000
POSTS_TO_LOAD = 500

BEGINNING = date.today().replace(day=1, month=1)
TODAY = timezone.now()
TIMEZONE = timezone.get_current_timezone()

def get_random_date(fake):
    return timezone.make_aware(fake.date_time_this_decade(), TIMEZONE)

def load_forums():
    Forum.objects.all().delete()
    for forum in FORUMS:
        f = Forum(
            name=forum[1],
        )
        f.save()
    return (str(len(FORUMS)) + ' forums loaded')

def load_threads(fake):
    Thread.objects.all().delete()
    forums = list(Forum.objects.all())
    users = list(User.objects.all())
    for i in range(THREADS_TO_LOAD):
        forum = random.choice(forums)
        forum.threads.create(
            subject = fake.sentence(),
            forum = forum,
            author = random.choice(users),
            pinned = random.randint(1, 8) == 5,
            closed = random.randint(1, 8) == 5,
        )
    return (str(THREADS_TO_LOAD) + ' threads loaded')

def load_thred_views(fake):
    View.objects.all().delete()
    threads = list(Thread.objects.all())
    users = list(User.objects.all())
    for i in range(THREAD_VIEWS_TO_LOAD):
        random.choice(threads).views.create(
            user = random.choice(users),
            ip = fake.ipv4(),
        )
    return (str(THREAD_VIEWS_TO_LOAD) + ' thread views loaded')

def load_posts(fake):
    Post.objects.all().delete()
    threads = list(Thread.objects.all())
    users = list(User.objects.all())
    for i in range(POSTS_TO_LOAD):
        thread = random.choice(threads)
        thread.posts.create(
            markdown = ' '.join(fake.paragraphs()),
            author = random.choice(users),
            thread = thread,
        )
    return (str(POSTS_TO_LOAD) + ' posts loaded')

def load_follows():
    users = User.objects.all()
    threads = Thread.objects.all()
    threads_set = set(threads)
    for user in users:
        selected_threads = random.sample(threads_set, random.randint(5,
            threads.count()))
        for thread in selected_threads:
            user.follows.create(user=user, thread=thread)
    return 'thread follows (subscriptions) loaded (30 to 100 per user)'

def load():
    fake = Faker()
    returns = []
    returns.append(load_forums())
    returns.append(load_threads(fake))
    returns.append(load_thred_views(fake))
    returns.append(load_posts(fake))
    returns.append(load_follows())
    return returns
