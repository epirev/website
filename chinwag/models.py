import difflib

from django.utils.text import slugify
from django.db import models

from easy_thumbnails.fields import ThumbnailerImageField as ImageField

from epirev.models import View
from accounts.models import User
from epirev.utils import mdrenderer

class PostEdit(models.Model):
    diff = models.TextField()
    author = models.ForeignKey(User, related_name='post_edit_author')
    date = models.DateTimeField(auto_now_add=True)

class Post(models.Model):
    content = models.TextField('Content')
    markdown = models.TextField('Markdown content')
    author = models.ForeignKey(User, related_name='post_author')
    date = models.DateTimeField('Date posted', auto_now_add=True)
    edits = models.ManyToManyField(PostEdit)
    thread = models.ForeignKey('Thread')

    new = False

    __markdown = None

    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)
        self.__markdown = self.markdown

    def save(self, *args, **kwargs):
        just_created = not self.pk
        if self.__markdown != self.markdown:
            self.content = mdrenderer.render(self.markdown)
            if not just_created:
                previous = self.__markdown.splitlines(True) if self.__markdown\
                        else ['']
                self.edits.create(
                    diff = difflib.unified_diff(previous,
                        self.markdown.splitlines(True)),
                    author = self.author,
                )
        super(Post, self).save(*args, **kwargs)
        if just_created:
            self.thread.last_post = self
            self.thread.save()

    def delete(self, *args, **kwargs):
        last_post = self.thread.posts.all()[:1]
        self.thread.last_post = last_post[0] if last_post else None
        self.thread.save()
        super(Post, self).delete(*args, **kwargs)

    def __str__(self):
        return '#' + str(self.pk)

    class Meta:
        ordering = ['-date']

# BEGIN THREAD

class Thread(models.Model):
    subject = models.CharField('Subject', max_length=140)
    slug_subject = models.SlugField(max_length=140)

    forum = models.ForeignKey('Forum', related_name='thread_forum')

    author = models.ForeignKey(User, related_name='thread_author')
    date = models.DateTimeField(auto_now_add=True)
    last_post = models.ForeignKey(Post, related_name='thread_last_post',
            null=True)

    posts = models.ManyToManyField(Post, related_name='thread_posts')
    views = models.ManyToManyField(View, related_name='thread_views')

    pinned = models.BooleanField(default=False)
    closed = models.BooleanField(default=False)

    new = False

    def save(self, *args, **kwargs):
        self.slug_subject = slugify(self.subject)
        just_created = not self.pk
        super(Thread, self).save(*args, **kwargs)
        if just_created:
            self.forum.save()

    def delete(self, *args, **kwargs):
        self.forum.save()
        super(Thread, self).delete(*args, **kwargs)

    def __str__(self):
        return self.subject

    class Meta:
        unique_together = ('forum', 'slug_subject')
        ordering = ['-pinned', '-last_post__date', '-date']

class Follow(models.Model):
    user = models.ForeignKey(User)
    thread = models.ForeignKey(Thread)
    canceled = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-thread__last_post__date']

# END THREAD

class Forum(models.Model):
    name = models.CharField(max_length=55)
    slug_name = models.CharField(max_length=55, unique=True)
    threads = models.ManyToManyField(Thread, related_name='forum_threads')
    project = models.ForeignKey('projects.Project', null=True, blank=True,
            related_name='forum_project')

    new = False
    pins = 0

    def save(self, *args, **kwargs):
        self.slug_name = slugify(self.name)
        super(Forum, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-project__end', 'name']

class LastSeen(models.Model):
    user = models.ForeignKey(User)
    forum = models.ForeignKey(Forum)
    thread = models.ForeignKey(Thread)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        index_together = [ ['user', 'forum'], ['user', 'thread'] ]
        ordering = ['-date']

    def __str__(self):
        return '<' + self.user.username + '> ' + str(self.date)
