from django.conf.urls import patterns, include, url
from django.contrib import admin

from chinwag.views import HotView
from chinwag.views import HotAjaxView
from chinwag.views import ForumView
from chinwag.views import ForumAjaxView
from chinwag.views import ThreadView

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', HotView.as_view(), name='hot'),
    url(r'^ajax/hot$', HotAjaxView.as_view(), name='hot_ajax'),
    url(r'^ajax/(?P<slug_name>[\w-]+)$', ForumAjaxView.as_view(),
        name='forum_ajax'),
    url(r'^(?P<slug_name>[\w-]+)$', ForumView.as_view(), name='forum'),
    url(r'^(?P<slug_name>[\w-]+)/(?P<slug_subject>[\w-]+)$',
        ThreadView.as_view(), name='thread'),
)
