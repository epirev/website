from django.views.generic.base import TemplateView
from blog.models import Article

class TimelineView(TemplateView):
    template_name = 'blog/timeline.html'
