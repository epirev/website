from django.conf.urls import patterns, include, url

from blog.views import TimelineView

urlpatterns = patterns('',
    url(r'^', TimelineView.as_view(), name='timeline'),
)
