from django.db import models
from accounts.models import User

class View(models.Model):
    article = models.ForeignKey('Article', related_name='article_views')
    ip = models.GenericIPAddressField(blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.ip

    class Meta:
        ordering = ['-date']

class Tag(models.Model):
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.name

class Article(models.Model):
    title = models.CharField(max_length=255)
    slug_title = models.SlugField(unique=True, blank=True, editable=False)
    content_html = models.TextField(blank=True, editable=False)
    content = models.TextField()
    author = models.ForeignKey(User)
    draft = models.BooleanField(default=True)
    date_published = models.DateField(auto_now_add=True)
    published_from = models.CharField(max_length=255)
    views = models.ManyToManyField(View, blank=True,
            related_name='article_views')
    views_count = models.PositiveIntegerField(default=0)
    tags = models.ManyToManyField(Tag)

    __was_draft = False

    def __str__(self):
        return self.title

    def __init__(self, *args, **kwargs):
        super(Article, self).__init__(*args, **kwargs)
        self.__was_draft = self.draft

    def save(self, *args, **kwargs):
        if (self.__was_draft and not self.draft):
            self.date_published = timezone.now()
        self.slug_title = slugify(self.title)
        self.abstract_html = mdrend.render(self.abstract)
        self.content_html = mdrend.render(self.content)
        super(Article, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-date_published']
