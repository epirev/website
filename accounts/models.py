from epirev import settings
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

class Title(models.Model):
    name = models.CharField(max_length=18)
    level = models.PositiveSmallIntegerField(default=0)
    color = models.CharField(max_length=10)

    def __str__(self):
        return self.name + ' (' + str(self.level) + ')'

    class Meta:
        ordering = ['-level', 'name']

class UserManager(BaseUserManager):
    def create_user(self, username, email, password):
        if not email:
            raise ValueError('Users must have an email')
        if not username:
            raise ValueError('Users must have an username')

        if not password:
            raise ValueError('Users must have a password')

        user = self.model(
            username=username,
            email=email,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username, email, password=password)
        user.is_admin = True
        user.is_active = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    username = models.CharField(
        verbose_name='username',
        max_length=20,
        unique=True,
    )
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
    )

    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    gender = models.CharField(max_length=20, choices=GENDER_CHOICES,
            default='Male')

    first_name = models.CharField(max_length=40, null=True, blank=True)
    last_name = models.CharField(max_length=40, null=True, blank=True)

    joined = models.DateTimeField(auto_now_add=True)

    login_x = models.CharField(max_length=20, unique=True)
    website = models.URLField(blank=True)
    school = models.CharField(max_length=40)
    projects = models.ManyToManyField('projects.Project',
            related_name='project_members', blank=True)

    titles = models.ManyToManyField(Title, blank=True)
    level = models.PositiveSmallIntegerField(default=0)

    follows = models.ManyToManyField('chinwag.Follow', blank=True,
            related_name='user_follows')

    email_follows = models.BooleanField(default=True)
    email_general = models.BooleanField(default=True)

    threads_per_page = models.PositiveSmallIntegerField(
            default=settings.CHINWAG_DEFAULT_THREADS_PER_PAGE)
    posts_per_page = models.PositiveSmallIntegerField(
            default=settings.CHINWAG_DEFAULT_POSTS_PER_PAGE)

    objects = UserManager()

    USERNAME_FIELD = 'username'

    REQUIRED_FIELDS = ['email']

    def save(self, *args, **kwargs):
        if self.pk and len(self.titles.all()) > 0:
            self.level = max(title.level for title in self.titles.all())
        super(User, self).save(*args, **kwargs)

    def get_full_name(self):
        if self.last_name and self.first_name:
            return self.first_name + ' ' + self.last_name
        return self.username

    def get_short_name(self):
        return self.username

    def __str__(self):
        return self.get_full_name()

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        return True

    def is_office(self):
        return self.level > 5

    def is_member(self):
        return self.level > 3

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_admin

    class Meta:
        ordering = ['-level', 'first_name', 'username', ]
