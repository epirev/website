from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^login/$', 'django.contrib.auth.views.login',
        { 'template_name': 'accounts/login.html' }, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', { 'next_page': '/' },
        name='logout'),
    url(r'^reset/$', 'django.contrib.auth.views.password_reset',
        { 'template_name': 'accounts/reset.html' }, name='password_reset'),
    url(r'^reset/done$', 'django.contrib.auth.views.password_reset_done',
        { 'template_name': 'accounts/reset_done.html' },
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm',
        { 'template_name': 'accounts/reset_confirm.html' },
        name='password_reset_confirm'),
    url(r'^reset/complete$',
        'django.contrib.auth.views.password_reset_complete',
        { 'template_name': 'accounts/reset_complete.html' },
        name='password_reset_complete'),
)
