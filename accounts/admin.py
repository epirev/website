from django.contrib import admin
from accounts import models
from django.contrib.auth.models import Group

class UserAdmin(admin.ModelAdmin):
    exclude = ('last_login', 'date_joined')

admin.site.register(models.Title)
admin.site.register(models.User)

admin.site.unregister(Group)
