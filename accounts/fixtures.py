import random

from faker import Faker

from accounts.models import Title, User


USERS_TO_LOAD = 17

SCHOOLS = ['EPITA', 'EPITECH', 'Gobelins Paris', 'ENS', 'SUP\'INFOGRAPH']

TITLES = [
        ('president',      9, '#dc685c'),
        ('vice-president', 8, '#35c687'),
        ('treasurer',      7, '#6f7dd0'),
        ('secretary',      6, '#e383ee'),
        ('dev',            5, '#74a3d3'),
        ('com',            4, '#8272d8'),
        ('tester',         3, '#9e9e9e'),
        ]

def get_login(first_name, last_name):
    """
    Returns the IONIS login given first_name and last_name
    """
    return last_name.lower()[:6] + '_' + first_name.lower()[:1]

def load_titles():
    Title.objects.all().delete()
    for t in TITLES:
        title = Title(
                name  = t[0],
                level = t[1],
                color = t[2],
                )
        title.save()

def load_superuser(fake):
    user = User(
        username    = 'toogy',
        is_active   = True,
        is_admin    = True,
        email       = 'toogy@too.gy',
        gender      = 'M',
        first_name  = 'Valentin',
        last_name   = 'Iovene',
        website     = 'http://too.gy',
        school      = 'EPITA',
        login_x     = 'iovene_v',
    )
    user.set_password('walter')
    user.save()
    user.titles.add(Title.objects.get(name='dev'))
    user.save()

def load_users(fake):
    User.objects.all().delete()
    load_superuser(fake)
    for i in range(USERS_TO_LOAD):
        user = User(
                username    = fake.user_name(),
                is_active   = True,
                is_admin    = False,
                email       = fake.email(),
                gender      = random.choice(User.GENDER_CHOICES)[0],
                first_name  = fake.first_name(),
                last_name   = fake.last_name(),
                website     = fake.url(),
                school      = random.choice(SCHOOLS),
                )
        user.login_x = get_login(user.first_name, user.last_name)
        user.set_password(fake.word())
        user.save()
    all_users = list(User.objects.all())
    office_titles = ['president', 'vice-president', 'treasurer', 'secretary']
    for t in office_titles:
        user = random.choice(all_users)
        all_users.remove(user)
        user.titles.add(Title.objects.get(name=t))
        if random.randint(1, 2) == 1:
            user.titles.add(Title.objects.get(name='dev'))
        user.save()
    for user in all_users:
        r = random.randint(1, 4)
        if r == 1:
            user.titles.add(Title.objects.get(name='dev'))
        elif r == 2:
            user.titles.add(Title.objects.get(name='com'))
        else:
            user.titles.add(Title.objects.get(name='tester'))
        user.save()

def load():
    results = []
    fake = Faker()
    load_titles()
    results.append(str(len(TITLES)) + ' titles loaded')
    load_users(fake)
    results.append(str(USERS_TO_LOAD) + ' users loaded (+ superuser)')
    return results

