# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20150131_0451'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='follows',
            field=models.ManyToManyField(related_name='user_follows', to='chinwag.Follow', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='joined',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='last_login',
            field=models.DateTimeField(null=True, verbose_name='last login', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='projects',
            field=models.ManyToManyField(related_name='project_members', to='projects.Project', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='titles',
            field=models.ManyToManyField(to='accounts.Title', blank=True),
        ),
    ]
