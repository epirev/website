# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '__first__'),
        ('chinwag', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(verbose_name='last login', default=django.utils.timezone.now)),
                ('username', models.CharField(verbose_name='username', unique=True, max_length=20)),
                ('email', models.EmailField(verbose_name='email address', max_length=255)),
                ('is_active', models.BooleanField(default=False)),
                ('is_admin', models.BooleanField(default=False)),
                ('gender', models.CharField(choices=[('M', 'Male'), ('F', 'Female')], default='Male', max_length=20)),
                ('first_name', models.CharField(null=True, blank=True, max_length=40)),
                ('last_name', models.CharField(null=True, blank=True, max_length=40)),
                ('joined', models.DateTimeField(default=datetime.datetime(2014, 11, 7, 21, 5, 46, 33615, tzinfo=utc))),
                ('login_x', models.CharField(max_length=20, unique=True)),
                ('website', models.URLField(blank=True)),
                ('school', models.CharField(max_length=40)),
                ('level', models.PositiveSmallIntegerField(default=0)),
                ('email_follows', models.BooleanField(default=True)),
                ('email_general', models.BooleanField(default=True)),
                ('threads_per_page', models.PositiveSmallIntegerField(default=20)),
                ('posts_per_page', models.PositiveSmallIntegerField(default=10)),
                ('follows', models.ManyToManyField(to='chinwag.Follow', null=True, blank=True, related_name='user_follows')),
                ('projects', models.ManyToManyField(to='projects.Project', null=True, blank=True, related_name='project_members')),
            ],
            options={
                'ordering': ['-level', 'first_name', 'username'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Title',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=18)),
                ('level', models.PositiveSmallIntegerField(default=0)),
                ('color', models.CharField(max_length=10)),
            ],
            options={
                'ordering': ['-level', 'name'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='user',
            name='titles',
            field=models.ManyToManyField(to='accounts.Title', null=True, blank=True),
            preserve_default=True,
        ),
    ]
