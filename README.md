# EPIREV's website

## Set up instructions to join the project

To set up the project:

+ first you'll need python 3.3 and use `virtualenv epirev` to create a virtual
environment for the project. (if you don't know about virtualenv, you should
learn)
+ enter the virtual environment using `source bin/activate` (you must be in the
directory created by the `virtualenv` command
+ then, using `pip`, install django and gunicorn (`pip install django gunicorn`)

> Some dependencies could be added during the project development, make sure to
> check that you installed all of them. (refer to the dependency list on the
> wiki)

+ finally, be sure that you are in the virtual environment root and clone this
repository (this should create a "epirev" directory

In the end, your directory structure should look like this:

```
/home/username/myprojects/epirev/ (virtualenv root)
    bin/
    epirev/
        README.md
        static/
        ...
    include/
    lib/
```

## Running the project

To run the project we use nginx and gunicorn. Nginx is used to proxy pass
gunicorn's stream and to serve static files. Here is an example of a working
nginx configuration:

```
upstream gunicorn-epirev {
    server localhost:4201 fail_timeout=0;
}

server {
    listen 80;
    server_name epirev;
    access_log /var/log/nginx/epirev.access.log;
    error_log /var/log/nginx/epirev.error.log;

    location /static/ {
        alias /srv/http/epirev/epirev/static/;
        expires 30d;
    }

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;
         
        proxy_pass http://gunicorn-epirev;
    }
}
```

Here gunicorn is running on localhost (127.0.0.1 or 0.0.0.0) and listening on
port 4201. Gunicorn's upstream is proxy passed through nginx, which is listening
on port 80.

To start gunicorn, you must run `gunicorn -b localhost:4201 epirev.wsgi`,
being in the project virtualenv (you must have `source ../bin/activate`) at the
root of the repo.

## Useful links

+ Django
    + [Django's source code](https://github.com/django/django)
    + [The Django Book](http://www.djangobook.com/en/2.0/index.html)
    + [Model fields and field options](https://docs.djangoproject.com/en/dev/ref/models/fields/)
    + [REST](http://www.django-rest-framework.org/)
    + [Translation](https://docs.djangoproject.com/en/dev/topics/i18n/translation/)
    + [Gravatar](https://github.com/nvie/django-gravatar)
    + [Fileupload](https://docs.djangoproject.com/en/dev/topics/http/file-uploads/)
    + [Authenticationsystem](https://docs.djangoproject.com/en/dev/topics/auth/default/)
    + [Django avatar](https://github.com/jezdez/django-avatar)

+ Gunicorn
    + [Deploying Gunicorn](http://docs.gunicorn.org/en/latest/deploy.html)
