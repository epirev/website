"""
Django settings for epirev project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'r^uh+n0@ki6mr23uyr&fv%0%e8)$5*3kq-_yp%6$2hl*@u1fb&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

TEMPLATE_DIRS = (
    BASE_DIR + '/templates',
)

ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'epirev.com']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'ipware',
    'easy_thumbnails',
    'debug_toolbar',
    'django_mathjax',

    'epirev',
    'avatar',
    'accounts',
    'chinwag',
    'projects',
    'blog',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'epirev.urls'

WSGI_APPLICATION = 'epirev.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'epirev',
        'USER': 'web',
        'PASSWORD': 'walter',
        'HOST': 'localhost',
        'PORT': '',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/home/static/epirev/'

STATICFILES_DIRS = (
    BASE_DIR + '/static/',
)

MEDIA_URL = '/media/'
MEDIA_ROOT = '/home/media/epirev/'

AUTH_USER_MODEL = 'accounts.User'

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    'epirev.utils.context_processors.get_view',
)

AVATAR_STORAGE_DIR = 'avatars/'
AVATAR_GRAVATAR_BACKUP = False
AVATAR_DEFAULT_URL = '/img/default_avatar.png'
AVATAR_DEFAULT_SIZE = 120

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_URL = '/logout/'

EMAIL_HOST           = 'epirev.com'
EMAIL_HOST_USER      = 'noreply@epirev.com'
EMAIL_HOST_PASSWORD  = 'hello'
EMAIL_PORT           = 587
EMAIL_SUBJECT_PREFIX = '[EPIREV]'
EMAIL_USE_TLS        = True
DEFAULT_FROM_EMAIL   = 'noreply@epirev.com'

PROJECTS_TIME_BETWEEN_VIEWS = 2400 # in seconds
THREADS_TIME_BETWEEN_VIEWS  = 2400 # in seconds

# Chinwag default configuration
CHINWAG_DEFAULT_THREADS_PER_PAGE = 20
CHINWAG_DEFAULT_POSTS_PER_PAGE = 10

MATHJAX_ENABLED=True
MATHJAX_CONFIG_FILE = "TeX-AMS-MML_HTMLorMML"
MATHJAX_CONFIG_DATA = {
    "tex2jax": {
        "inlineMath": [ ['$','$'], ['\\(','\\)'] ],
    },
    "showMathMenu": False,
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

import epirev
try:
    from epirev.settings_local import *
except ImportError:
    pass
