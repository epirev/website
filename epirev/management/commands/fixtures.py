import sys
import importlib

from django.core.management.base import BaseCommand, CommandError

# Some colors
OKGREEN = '\033[92m'
ENDC    = '\033[0m'
WARNING = '\033[93m'
SUC_PX  = '[success] '
WARN_PX = '[warning] '

def succ(string):
    return OKGREEN + SUC_PX + ENDC + string

def warn(string):
    return WARNING + WARN_PX + ENDC + string

MODULES = [
    'accounts',
    'chinwag',
    'projects',
    'blog',
    'epirev',
]

class Command(BaseCommand):
    args = '<module module ...>'
    help = 'File the database with fake data for the specified modules'

    def handle(self, *args, **kwargs):
        if len(args) < 1:
            raise CommandError('Missing parameters')
        if args[0] == 'all':
            args = MODULES
        for module in args:
            if module not in MODULES:
                raise CommandError('%s: unknown module' % module)
            if not importlib.util.find_spec(module):
                raise CommandError('%s: module not loaded' % module)
        for module in args:
            fixtures_module = module + '.fixtures'
            try:
                fixtures = importlib.import_module(fixtures_module)
                if not hasattr(fixtures, 'load'):
                    self.stderr.write(warn('%s.fixtures: module does NOT '
                            'implement the required "load" function' % module))
                    continue
                results = fixtures.load()
                prefix = '%s: ' % module
                spaces = ' ' * (len(prefix) + len(WARN_PX))
                prefix = succ(prefix)
                if results:
                    self.stdout.write(prefix + results[0])
                    for i in range(1, len(results)):
                        self.stdout.write(spaces + results[i])
                    self.stdout.flush()
            except ImportError as e:
                self.stdout.write(warn('%s: module doesn\'t have fixtures'
                        % module))

