from django.db import models

from accounts.models import User

class View(models.Model):
    user = models.ForeignKey(User, related_name='view_user', null=True,
            blank=True)
    date = models.DateTimeField(auto_now_add=True)
    ip = models.GenericIPAddressField(null=True)

    def __str__(self):
        if self.user:
            return self.user.username + ' (' + self.ip + ')'
        else:
            return self.ip

    class Meta:
        ordering = ['-date']

