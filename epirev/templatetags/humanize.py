from datetime import datetime, timedelta, date
from django import template
from django.utils import timezone
from django.utils.timesince import timesince

register = template.Library()

@register.filter
def date_ago(value):
    today = date.today()
    #try:
    difference = today - value
    #except:
        #return timesince(value) + ' ago'

    if difference <= timedelta(minutes=1):
        return 'today'
    elif difference <= timedelta(days=1):
        return 'yesterday'
    return '%(time)s ago' % {'time': timesince(value).split(', ')[0]}

@register.filter
def datetime_ago(value):
    return '%(time)s ago' % {'time': timesince(value).split(', ')[0]}
