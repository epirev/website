from django.core.urlresolvers import resolve

def get_view(request):
    view = resolve(request.path)
    return {
        'view_name': view.url_name,
        'app_name': view.app_name
    }
