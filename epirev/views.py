from django.shortcuts import render, redirect

from accounts.models import User

def home(request):
    return redirect('projects:all')

def contact(request):
    return render(request, 'epirev/contact.html')

def team(request):
    members = User.objects \
        .filter(is_active=True, level__gte=3) \
        .prefetch_related('titles')
    return render(request, 'epirev/team.html', { 'members': members })
