from django.conf.urls import patterns, include, url

from django.contrib import admin

import debug_toolbar

#import debug_toolbar

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'epirev.views.home', name='home'),
    url(r'^blog/', include('blog.urls', app_name='blog', namespace='blog')),
    url(r'^project/', include('projects.urls', app_name='projects',
        namespace='projects')),
    url(r'^forum/', include('chinwag.urls', app_name='chinwag',
        namespace='chinwag')),

    url(r'^contact/$', 'epirev.views.contact', name='contact'),
    url(r'^team/$', 'epirev.views.team', name='team'),

    url(r'', include('accounts.urls', app_name='accounts',
                     namespace='accounts')),

    url(r'^avatar/', include('avatar.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^__debug__/', include(debug_toolbar.urls)),
)
