$(document).ready(function() {
    if ($('#user-nav > ul > li').length == 0) {
        $('#main-nav').removeClass('logged-in');
        $('#user-nav').hide();
    }

    $('#chinwag-nav .item > a').click(function(e) {
        $('#chinwag-nav .item').removeClass('active');
        $(this).parent().addClass('active');
    });
});

$(function() {
    $('body').on('click', '.forum-ajax-trigger', function(e) {
        e.preventDefault();
        var ajax_url = $(this).attr('data');
        var url = $(this).attr('href');
        $.get(ajax_url, function(data) {
            $('#forum-ajax').html(data);
        });
        window.history.pushState('', '', url);
    });
});

