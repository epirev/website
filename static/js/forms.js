$(document).ready(function() {
    $('.form-field input, .form-field textarea').focus(function() {
        $(this).parent().addClass('active');
    });
    $('.form-field input, .form-field textarea').blur(function() {
        $(this).parent().removeClass('active');
    });
});
